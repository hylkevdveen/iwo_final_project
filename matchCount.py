# !/usr/bin/python3

# Title: matchCount.py
# Author: H. van der Veen
# Date: 20-03-2018

import sys
import gzip


def match_count(argv):
    """Print the match count of token (argv[2]) in Google Books corpora
zip file (argv[1]) as following: <word> <pre 1945> <post 1945>"""

    prewar = 0
    postwar = 0

    with gzip.open(argv[1]) as f:
        for line in f:
            line = line.decode("UTF-8")
            line = line.split('\t')
            if argv[2].lower() == line[0].lower():
                if 1900 < int(line[1]) <= 1945:
                    prewar += int(line[2])
                elif 1945 < int(line[1]) <= 2000:
                    postwar += int(line[2])

    print("{0} {1} {2}".format(argv[2], prewar, postwar))


if __name__ == "__main__":
    match_count(sys.argv)
